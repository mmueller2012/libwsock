#include <wsock/tcpsock.h>
#include <wsock/websocket.h>
#include <wsock/workqueue.h>
#include <iostream>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

static inline std::string intToString(int x)
{
    std::stringstream ss;
    ss << x;
    return ss.str();
}

static bool stringBeginsWith(std::string str, std::string start)
{
    if (str.length() < start.length())
        return false;
    return !strncmp(str.c_str(), start.c_str(), start.length());
}

class JobItem : public WorkItem
{
    private:
        std::string name;

    public:
        JobItem(std::string name)
        {
            this->name = name;
        }

        std::string getName()
        {
            return this->name;
        }
};

WorkQueue jobqueue = WorkQueue();

class WebSocketClient : public WebSocket
{
    virtual TCPSocket* create()
    {
        return new WebSocketClient;
    }

    virtual void onHTTPRequest(std::string url, const std::map<std::string, std::string> &parameters, const std::map<std::string, std::string> &headerFields)
    {
        this->sendHTTPResponseString(404, "Not Found", "text/plain", "This is a websocket server!");
    }

    virtual bool websocketAcceptConnection(std::string url, const std::map<std::string, std::string> &parameters, const std::map<std::string, std::string> &headerFields)
    {
        return true;
    }

    virtual void onWebsocketConnect()
    {
        std::cout << "New Websocket Client with IP " << this->getIPAddress() << std::endl;
    }

    virtual void onWebsocketData(bool text, unsigned char *data, int length)
    {
        if (!text)
        {
            std::cout << "Ignoring binary data" << std::endl;
            return;
        }

        std::string str = std::string((char*)data, length);

        if (stringBeginsWith(str, "newjob:"))
        {
            std::string job_name = str.substr(7);

            JobItem *job = new JobItem(job_name);
            jobqueue.appendWork(job);

            this->sendWebsocketString("Added new job " + job_name);
        }
        else if (str == "getjob")
        {
            JobItem *job = (JobItem *)jobqueue.tryGetWork(this, 1);
            if (job)
            {
                this->sendWebsocketString("Got job: " + job->getName());
                job->decRef();
            }
            else
                this->sendWebsocketString("No job available, waiting for the next added one");
        }
    }

    virtual void onTimeout(int timerid, void *userdata)
    {
        if (timerid == 1)
        {
            JobItem *job = (JobItem *)userdata;
            this->sendWebsocketString("Got newly added job: " + job->getName());
            job->decRef();
        }
    }

    virtual void onWebsocketDisconnect()
    {
        std::cout << "WebSocket Client disconnected" << std::endl;
        this->deleteTimers();
    }

    virtual ~WebSocketClient()
    {
        std::cout << "Destructed " << this << std::endl;
    }
};

int main()
{
    signal(SIGPIPE, SIG_IGN);

    TCPServer server(new WebSocketClient, 8377);

    if (server.start())
    {
        server.handleEvents();
        std::cerr << "Server managed to break out from endless loop?!" << std::endl;
    }
    else
    {
        std::cerr << "Failed to start server!" << std::endl;
    }
}
