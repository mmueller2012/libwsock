#include <wsock/tcpsock.h>
#include <wsock/websocket.h>
#include <iostream>
#include <ctype.h>
#include <stdlib.h>
#include <signal.h>

static std::string intToString(int x)
{
	std::stringstream ss;
	ss << x;
	return ss.str();
}

class WebSocketClient : public WebSocket
{

	virtual TCPSocket* create()
	{
		return new WebSocketClient;
	}

	virtual void onHTTPRequest(std::string url, const std::map<std::string, std::string> &parameters, const std::map<std::string, std::string> &headerFields)
	{
		this->sendHTTPResponseString(404, "Not Found", "text/plain", "This is a websocket server!");
	}

	virtual bool websocketAcceptConnection(std::string url, const std::map<std::string, std::string> &parameters, const std::map<std::string, std::string> &headerFields)
	{
		return true;
	}

	virtual void onWebsocketConnect()
	{
		std::cout << "New Websocket Client with IP " << this->getIPAddress() << std::endl;

		/* first message after five seconds */
		this->registerTimer(5000, 1, NULL);
	}

	virtual void onWebsocketData(bool text, unsigned char *data, int length)
	{
		std::cout << "Received data" << std::endl;
		this->sendWebsocketData(text, data, length);
	}

	virtual void onTimeout(int timerid, void *userdata)
	{
		std::cout << "Timeout of timer " << timerid << " (userdata=" << userdata << ")" << std::endl;
		this->sendWebsocketString("Hello World " + intToString(timerid));

		/* re-register the timeout if we are still connected */
		if (this->isConnected())
			this->registerTimer(5000, timerid + 1, NULL);
	}

	virtual void onWebsocketDisconnect()
	{
		std::cout << "WebSocket Client disconnected" << std::endl;
		this->deleteTimers();
	}

	virtual ~WebSocketClient()
	{
		std::cout << "Destructed " << this << std::endl;
	}
};

int main()
{
	signal(SIGPIPE, SIG_IGN);

	TCPServer server(new WebSocketClient, 8377);

	if (server.start())
	{
		server.handleEvents();
		std::cerr << "Server managed to break out from endless loop?!" << std::endl;
	}
	else
	{
		std::cerr << "Failed to start server!" << std::endl;
	}
}