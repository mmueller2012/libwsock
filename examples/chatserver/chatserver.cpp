#include <wsock/tcpsock.h>
#include <wsock/websocket.h>
#include <iostream>
#include <list>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

static inline std::string intToString(int x)
{
    std::stringstream ss;
    ss << x;
    return ss.str();
}

class WebSocketClient;

std::list<WebSocketClient*> users;

static bool stringBeginsWith(std::string str, std::string start)
{
    if (str.length() < start.length())
        return false;
    return !strncmp(str.c_str(), start.c_str(), start.length());
}

class WebSocketClient : public WebSocket
{
    std::string username;
    std::list<WebSocketClient*>::iterator it = users.end();

    virtual TCPSocket* create()
    {
        return new WebSocketClient;
    }

    virtual void onHTTPRequest(std::string url, const std::map<std::string, std::string> &parameters, const std::map<std::string, std::string> &headerFields)
    {
        this->sendHTTPResponseString(404, "Not Found", "text/plain", "This is a websocket server!");
    }

    virtual bool websocketAcceptConnection(std::string url, const std::map<std::string, std::string> &parameters, const std::map<std::string, std::string> &headerFields)
    {
        return true;
    }

    virtual void onWebsocketConnect()
    {
        std::cout << "New Websocket Client with IP " << this->getIPAddress() << std::endl;
    }

    virtual void onWebsocketData(bool text, unsigned char *data, int length)
    {
        if (!text)
        {
            std::cout << "Ignoring binary data" << std::endl;
            return;
        }

        std::string str = std::string((char*)data, length);

        if (stringBeginsWith(str, "nick:"))
        {
            std::string new_nick = str.substr(5);

            if (!this->username.empty() || new_nick.empty()) return;
            if (this->it != users.end()) return;

            std::cout << "New user " << new_nick << std::endl;

            this->username = new_nick;
            this->it = users.insert(users.begin(), this);

            for (auto user_it = users.begin(); user_it != users.end(); user_it++)
            {
                std::string new_user;

                new_user = "newuser:" + this->username;
                (*user_it)->sendWebsocketData(true, (unsigned char*)new_user.c_str(), new_user.length());

                if (this->it != user_it)
                {
                    new_user = "newuser:" + (*user_it)->username;
                    this->sendWebsocketData(true, (unsigned char*)new_user.c_str(), new_user.length());
                }
            }
        }
        else if (stringBeginsWith(str, "msg:"))
        {
            if (this->it == users.end()) return;

            std::string new_msg = "msg:<" + this->username + ">: " + str.substr(4);
            for (auto user_it = users.begin(); user_it != users.end(); user_it++)
                (*user_it)->sendWebsocketData(true, (unsigned char*)new_msg.c_str(), new_msg.length());
        }
    }

    virtual void onTimeout(int timerid, void *userdata)
    {
        return;
    }

    virtual void onWebsocketDisconnect()
    {
        std::cout << "WebSocket Client disconnected" << std::endl;
        if (this->it != users.end())
        {
            std::string del_user = "deluser:" + this->username;

            for (auto user_it = users.begin(); user_it != users.end(); user_it++)
            {
                if (this->it == user_it)
                    continue;
                (*user_it)->sendWebsocketData(true, (unsigned char*)del_user.c_str(), del_user.length());
            }
            users.erase(this->it);
        }
        this->deleteTimers();
    }

    virtual ~WebSocketClient()
    {
        std::cout << "Destructed " << this << std::endl;
    }
};

int main()
{
    signal(SIGPIPE, SIG_IGN);

    TCPServer server(new WebSocketClient, 8377);

    if (server.start())
    {
        server.handleEvents();
        std::cerr << "Server managed to break out from endless loop?!" << std::endl;
    }
    else
    {
        std::cerr << "Failed to start server!" << std::endl;
    }
}
