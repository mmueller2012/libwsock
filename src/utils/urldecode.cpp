#include <string>
#include <sstream>

inline int ishex(char x)
{
    return  (x >= '0' && x <= '9')  || (x >= 'a' && x <= 'f') || (x >= 'A' && x <= 'F');
}

char hexToChr(char c1, char c2)
{
    unsigned int x;
    std::stringstream ss;
    ss << std::hex << c1 << c2;
    ss >> x;
    return (char)x;
}

bool urlDecode(std::string input, std::string &output)
{
    unsigned int i = 0;
    unsigned int j = 0;

    output.resize(input.size());

    while (i < input.size())
    {
        if (input[i] == '+')
        {
            output[j] = ' ';
            i++;
            j++;
        }
        else if (input[i] == '%')
        {
            if (i > input.size() - 3)
                return false;

            if (!ishex(input[i+1]) || !ishex(input[i+2]))
                return false;

            output[j] = hexToChr(input[i+1], input[i+2]);

            i += 3;
            j += 1;
        }
        else
        {
            output[j] = input[i];
            i++;
            j++;
        }
    }

    output.resize(j);

    return true;
}