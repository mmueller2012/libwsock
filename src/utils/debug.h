#include <stdio.h>

static inline void hexdump(const char *label, void *addr, int len)
{
    unsigned char *buf = (unsigned char*)addr;
    int i, j;

    if (label != NULL)
        printf("%s:\n", label);

    for (i = 0; i < len; i += 16)
    {
        printf("%08x: ", i);

        /* hex code */
        for (j = 0; j < 16; j++)
            if (i + j < len)
                printf("%02x ", buf[i+j]);
            else
                printf("   ");

        printf(" ");

        /* chars */
        for (j = 0; j < 16; j++)
            if (i + j < len)
                printf("%c", isprint(buf[i+j]) ? buf[i+j] : '.');

        printf("\n");
    }
}
