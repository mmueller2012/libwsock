#include <assert.h>
#include "stdio.h"
#include "stdlib.h"

#include "base64.h"
#include "checkutf8.h"
#include "swap.h"
#include "urldecode.h"

void test_base64()
{
    unsigned char str[] = "any carnal pleasure.";

    assert(base64Encode(str, 20) == "YW55IGNhcm5hbCBwbGVhc3VyZS4=");
    assert(base64Encode(str, 19) == "YW55IGNhcm5hbCBwbGVhc3VyZQ==");
    assert(base64Encode(str, 18) == "YW55IGNhcm5hbCBwbGVhc3Vy");
    assert(base64Encode(str, 17) == "YW55IGNhcm5hbCBwbGVhc3U=");
    assert(base64Encode(str, 16) == "YW55IGNhcm5hbCBwbGVhcw==");

}

void test_checkutf8()
{

    /* Take a look here for some UTF tests: http://www.cl.cam.ac.uk/~mgk25/ucs/examples/ */

    unsigned char test001[] = "Hello World.";
    assert(isValidUTF8(test001, sizeof(test001)));

    unsigned char test002[] = {'H', 'a', 0x80, 'l', 'l', 'o', 0};
    assert(!isValidUTF8(test002, sizeof(test002)));

    unsigned char test003[] = {0xed, 0x9f, 0xbf};
    assert(isValidUTF8(test003, sizeof(test003)));
    assert(!isValidUTF8(test003, sizeof(test003) - 1));

    unsigned char test004[] = {0xee, 0x80, 0x80};
    assert(isValidUTF8(test004, sizeof(test004)));

    unsigned char test005[] = {0xef, 0xbf, 0xbd};
    assert(isValidUTF8(test005, sizeof(test005)));

    unsigned char test006[] = {0xf4, 0x8f, 0xbf, 0xbf};
    assert(isValidUTF8(test006, sizeof(test006)));

    /*
    unsigned char test007[] = {0xf4, 0x90, 0x80, 0x80};
    assert(isValidUTF8(test007, sizeof(test007)));
    */

    unsigned char test008[] = {0xfe};
    assert(!isValidUTF8(test008, sizeof(test008)));

    unsigned char test009[] = {0xff};
    assert(!isValidUTF8(test009, sizeof(test009)));

    unsigned char test010[] = {0xfe, 0xfe, 0xff, 0xff};
    assert(!isValidUTF8(test010, sizeof(test010)));

    unsigned char test011[] = {0xfc, 0x80, 0x80, 0x80, 0x80, 0xaf};
    assert(!isValidUTF8(test011, sizeof(test011)));

    unsigned char test012[] = {0xc0, 0x80};
    assert(!isValidUTF8(test012, sizeof(test012)));

    unsigned char test013[] = {0xce, 0xba, 0xe1, 0xbd, 0xb9, 0xcf, 0x83, 0xce, 0xbc, 0xce, 0xb5, 0xed, 0xa0, 0x80, 0x65, 0x64, 0x69, 0x74, 0x65, 0x64};
    assert(!isValidUTF8(test013, sizeof(test013)));

    FILE *fp = fopen("UTF-8-demo.txt", "rb");
    if (fp)
    {
        fseek(fp, 0, SEEK_END);
        long len = ftell(fp);
        fseek(fp, 0, SEEK_SET);

        unsigned char *buffer = (unsigned char *)malloc(len + 1);
        assert(buffer);
        buffer[len] = 0;

        assert((signed)fread(buffer, 1, len, fp) == len);
        fclose(fp);

        assert(isValidUTF8(buffer, len));
        free(buffer);
    }
    else
        fprintf(stderr, "Skipping test, no UTF-8-demo.txt file found.\n");

}

void test_bswap()
{
    assert(bswap16(0x1122) == 0x2211);
    assert(bswap32(0x11223344) == 0x44332211);
    assert(bswap64(0x1122334455667788) == 0x8877665544332211);

    /* TODO: make big endian compatible! */
    uint32_t test = 0x11223344;
    assert(*(uint8_t *)&test == 0x44);

}

struct URLTest{
    bool result;
    std::string input;
    std::string expectedOutput;
};

URLTest urlTests[] =
{
    {true, "http%3A%2F%2Ffoo%20bar%2F", "http://foo bar/"},
    {false, "abc%0", ""}
};

void test_urldecode()
{
    std::string output;
    for (unsigned int i = 0; i < sizeof(urlTests) / sizeof(urlTests[0]); i++)
    {
        assert(urlDecode(urlTests[i].input, output) == urlTests[i].result);
        if (urlTests[i].result)
            assert(urlTests[i].expectedOutput == output);
    }
}


int main()
{
    test_base64();
    test_checkutf8();
    test_bswap();
    test_urldecode();

    return 0;
}