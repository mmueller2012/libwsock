#include "base64.h"
#include <algorithm>

static unsigned char base64Table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

std::string base64Encode(const unsigned char *data, int length)
{
    unsigned int tempInt;
    char temp[4];
    std::string result;

    for (;length >= 3; data += 3, length -= 3)
    {
        tempInt  = (data[0] << 16);
        tempInt |= (data[1] << 8);
        tempInt |=  data[2];

        for (int i = 0; i < 4; i++)
            temp[i] = base64Table[(tempInt >> (18 - i * 6)) & 63];

        result.append( std::string(temp, 4) );
    }

    if (length >= 1)
    {
        tempInt  = (data[0] << 16);
        if (length >= 2)
            tempInt |= (data[1] << 8);

        for (int i = 0; i < 3; i++)
            temp[i] = base64Table[(tempInt >> (18 - i * 6)) & 63];
        if (length < 2)
            temp[2] = '=';
        temp[3] = '=';

        result.append( std::string(temp, 4) );
    }

    return result;
}
