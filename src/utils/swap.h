#include "stdint.h"
#include "stdio.h"
#include <ctype.h>

static inline uint16_t bswap16( uint16_t a )
{
    return (a<<8) | (a>>8);
}

static inline uint32_t bswap32( uint32_t a )
{
    return ((uint32_t)bswap16(a)<<16) | bswap16(a>>16);
}

static inline uint64_t bswap64( uint64_t a )
{
    return ((uint64_t)bswap32(a)<<32) | bswap32(a>>32);
}
