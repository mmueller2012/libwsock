#include "checkutf8.h"
#include "stdio.h"

static inline int get_utf8_continuation_byte(unsigned char p)
{
    return ((p & 0xC0) == 0x80) ? (p & 0x3F) : (-1);
}

bool isValidUTF8(unsigned char *str, int len)
{
    int c0, c1, c2, c3, r;
    int len_sequence = 0;

    while (len-- > 0)
    {
        c0 = *str++;

        if ((c0 & 0x80) == 0)
            continue;
        else if ((c0 & 0xE0) == 0xC0)
            len_sequence = 1;
        else if ((c0 & 0xF0) == 0xE0)
            len_sequence = 2;
        else if ((c0 & 0xF8) == 0xF0)
            len_sequence = 3;
        else
            return false;

        if (len < len_sequence)
            return false;

        if (len_sequence == 1)
        {
            c1 = get_utf8_continuation_byte(str[0]);
            if (c1 == -1)
                return false;
            r = ((c0 & 0x1F) << 6) | c1;
            if (r < 128)
                return false;           
        }
        else if (len_sequence == 2)
        {
            c1 = get_utf8_continuation_byte(str[0]);
            c2 = get_utf8_continuation_byte(str[1]);
            if ((c1 | c2) == -1)
                return false;
            r = ((c0 & 0x0F) << 12) | (c1 << 6) | c2;
            if (r < 2048 || (r >= 0xD800 && r <= 0xDFFF))
                return false;

        }
        else if (len_sequence == 3)
        {
            c1 = get_utf8_continuation_byte(str[0]);
            c2 = get_utf8_continuation_byte(str[1]);
            c3 = get_utf8_continuation_byte(str[1]);
            if ((c1 | c2 | c3) == -1)
                return false;
            r = ((c0 & 0x07) << 18) | (c1 << 12) | (c2 << 6) | c3;
            if (r < 0x10000 || r > 0x10FFFF)
                return false;
        }

        len -= len_sequence;
        str += len_sequence;
    }

    return true;
}
