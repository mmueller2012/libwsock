#include "wsock/workqueue.h"

#include <sys/eventfd.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#ifdef NDEBUG
    #define expect(cond) ((void)(cond))
#else
    #define expect(cond) assert(cond)
#endif

/*
    --------------- WorkItem ---------------
*/

#define WAITERS_SIGNALLED 0x80000000

WorkItem::WorkItem()
{
    this->refcount  = 1;
    this->waiters   = 0;
    this->event     = ::eventfd(0, EFD_SEMAPHORE);

    /* TODO: how to proceed if no eventfd's are left? */
    assert(this->event >= 0);
}

WorkItem::~WorkItem()
{
    ::close(this->event);
    assert((this->waiters & ~WAITERS_SIGNALLED) == 0);
    assert(this->refcount == 0);
}

void WorkItem::incRef()
{
    __sync_add_and_fetch(&this->refcount, 1);
}

void WorkItem::decRef()
{
    if (__sync_add_and_fetch(&this->refcount, -1) <= 0)
        delete this;
}

/* check if the element is already signalled */
bool WorkItem::isSignalled()
{
    return ((this->waiters & WAITERS_SIGNALLED) != 0);
}

/* atomically check if the event is signalled, otherwise wait for the event */
void WorkItem::wait()
{
    unsigned int tmp;
    uint64_t dummy;
    int res;

    do
    {
        tmp = this->waiters;
        if (tmp & WAITERS_SIGNALLED) return;
    }
    while (!__sync_bool_compare_and_swap(&this->waiters, tmp, tmp + 1));

    /* if we have only one reference at this point there is no other thread that can signal the event */
    assert(this->refcount > 1 || (this->waiters & WAITERS_SIGNALLED) != 0);

    /* wait for item to get ready */
    res = read(this->event, &dummy, sizeof(dummy));
    assert(res == sizeof(dummy));
    assert(dummy == 1);

    /* this->waiters & WAITERS_SIGNALLED should be 1 now, but we don't check, as
     * there is a small chance, that a different thread has resetted it in the meantime. */
}

/* wake up all waiting threads, prevent new threads from waiting */
void WorkItem::signal()
{
    unsigned int tmp;
    uint64_t count;
    int res;

    do
    {
        tmp = this->waiters;
        if (tmp & WAITERS_SIGNALLED) return;
    }
    while (!__sync_bool_compare_and_swap(&this->waiters, tmp, WAITERS_SIGNALLED));

    /* increment the value of the eventfd */
    count = tmp;
    res = write(this->event, &count, sizeof(count));
    assert(res == sizeof(count));
}

/* resets the work item, so that it can be processed again */
void WorkItem::reset()
{
    unsigned int tmp;

    do
    {
        tmp = this->waiters;
        if (!(tmp & WAITERS_SIGNALLED)) return;
    }
    while (!__sync_bool_compare_and_swap(&this->waiters, tmp, tmp & ~WAITERS_SIGNALLED));
}

/*
    --------------- WorkQueue ---------------
*/

WorkQueue::WorkQueue()
{
    pthread_mutex_init(&this->queueLock, NULL);
    this->queueList.clear();
    this->waitList.clear();
}

WorkQueue::~WorkQueue()
{
    /* nothing to do */
}

void WorkQueue::appendWork(WorkItem *item)
{
    item->reset();
    item->incRef();
    this->requeueWork(item);
}

void WorkQueue::requeueWork(WorkItem *item)
{
    std::list<WaitEntry>::iterator it;

    expect(!pthread_mutex_lock(&this->queueLock));

    it = this->waitList.begin();
    if (it != this->waitList.end())
    {
        it->client->registerTimer(0, it->timerid, item);
        it->client->decRef();

        this->waitList.erase(it);
    }
    else
        this->queueList.push_back(item);

    expect(!pthread_mutex_unlock(&this->queueLock));
}

WorkItem* WorkQueue::tryGetWork(TCPSocket *client, int timerid)
{
    std::list<WorkItem*>::iterator it;
    WorkItem *item = NULL;

    expect(!pthread_mutex_lock(&this->queueLock));

    it = this->queueList.begin();
    if (it != this->queueList.end())
    {
        item = *it;

        this->queueList.erase(it);
    }
    else if (client != NULL)
    {
        WaitEntry entry;
        entry.client    = client;
        entry.timerid   = timerid;

        client->incRef();

        this->waitList.push_back(entry);
    }

    expect(!pthread_mutex_unlock(&this->queueLock));
    return item;
}

int WorkQueue::deleteTimers(TCPSocket *client, int timerid)
{
    std::list<WaitEntry>::iterator it;
    int numDeletedTimers = 0;

    expect(!pthread_mutex_lock(&this->queueLock));

    for (it = this->waitList.begin(); it != this->waitList.end();)
    {
        if (it->client == client && (it->timerid == timerid || timerid == -1))
        {
            it->client->decRef();
            it = this->waitList.erase(it);
            numDeletedTimers++;
        }
        else
            it++;
    }

    expect(!pthread_mutex_unlock(&this->queueLock));
    return numDeletedTimers;
}
