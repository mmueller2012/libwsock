#include "wsock/tcpsock.h"

#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <stdio.h>

#include <iostream>
#include <algorithm>
#include <sstream>

#define MAX_EVENTS 100

static std::string intToString(int x)
{
    std::stringstream ss;
    ss << x;
    return ss.str();
}

#ifdef NDEBUG
    #define expect(cond) ((void)(cond))
#else
    #define expect(cond) assert(cond)
#endif

/***********************************************************************
 *                               TODOS
 *
 * - Add possibility to wait for timeout. To do this TCP server should
 *   maintain a timer queue and set the epool timeout to the next event
 *   in the linked list. A timeout is necessary to detect an inactive
 *   connection (but browser still open).
 *
 */

/*
    --------------- TCPServer ---------------
*/

TCPServer::TCPServer(TCPSocket *clientClass, int port, bool onlyIPv4)
{
    this->mainThread  = pthread_self();

    this->clientClass = clientClass;
    this->socket      = -1;
    this->port        = port;
    this->onlyIPv4    = onlyIPv4;
    this->started     = false;
    this->epollList   = -1;
    this->eventClients.clear();

    this->timerEvent  = -1;
    pthread_mutex_init(&this->timerLock, NULL);
    this->timerList.clear();
}

TCPServer::~TCPServer(){
    if (this->started)
        this->stop();

    if (this->clientClass)
        this->clientClass->decRef();
}

/* accepts a TCP connection, only used internally */
void TCPServer::acceptConnection()
{
    if (!this->started)
        return;

    int clientSocket = ::accept(this->socket, NULL, NULL);
    if (clientSocket < 0)
        return;

    TCPSocket *newClient = this->clientClass->create();
    if (newClient)
    {
        newClient->socket = clientSocket;
        newClient->server = this;

        if (newClient->init())
        {
            newClient->onConnect();
            newClient->decRef();
            return;
        }

        newClient->decRef();
    }
    ::close(clientSocket);
}

bool TCPServer::changeEventListener(TCPSocket *client, uint32_t events)
{
    if (!this->started)
        return false;

    struct epoll_event event;
    memset(&event, 0, sizeof(event));
    event.events   = events;
    event.data.ptr = (void *)client;

    int op = (this->eventClients.find(client) != this->eventClients.end()) ? EPOLL_CTL_MOD : EPOLL_CTL_ADD;
    if (op == EPOLL_CTL_ADD)
    {
        client->incRef();
        this->eventClients.insert(client);
    }

    return (::epoll_ctl(this->epollList, op, client->socket, &event) == 0);
}

void TCPServer::removeEventListener(TCPSocket *client)
{
    if (!this->started)
        return;

    std::set<TCPSocket*>::iterator it = this->eventClients.find(client);
    if (it == this->eventClients.end())
        return;

    /* not used, but kernels before 2.6.9 need this field to be set */
    struct epoll_event event;
    memset(&event, 0, sizeof(event));
    event.events = EPOLLIN;

    ::epoll_ctl(this->epollList, EPOLL_CTL_DEL, client->socket, &event);

    this->eventClients.erase(it);
    client->decRef();
}

int TCPServer::getNextTimeout(int timeout)
{
    std::list<TimerEntry>::iterator it;
    struct timespec monotime;
    unsigned long long timestamp;

    expect(!clock_gettime(CLOCK_MONOTONIC, &monotime));
    timestamp = monotime.tv_sec * (unsigned long long)1000 + monotime.tv_nsec / 1000000;

    expect(!pthread_mutex_lock(&this->timerLock));

    it = this->timerList.begin();
    if (it != this->timerList.end())
    {
        if (timestamp >= it->timestamp)
            timeout = 0;
        else if (timeout == -1)
            timeout = it->timestamp - timestamp;
        else
            timeout = std::min(timeout, (int)(it->timestamp - timestamp));
    }

    expect(!pthread_mutex_unlock(&this->timerLock));
    return timeout;
}

void TCPServer::handleTimerEvents()
{
    std::list<TimerEntry>::iterator it;
    struct timespec monotime;
    unsigned long long timestamp;
    TimerEntry entry;

    expect(!clock_gettime(CLOCK_MONOTONIC, &monotime));
    timestamp = monotime.tv_sec * (unsigned long long)1000 + monotime.tv_nsec / 1000000;

    expect(!pthread_mutex_lock(&this->timerLock));

    for (;;)
    {
        it = this->timerList.begin();
        if (it == this->timerList.end())
            break;
        if (it->timestamp > timestamp)
            break;

        entry = *it;
        this->timerList.erase(it);

        expect(!pthread_mutex_unlock(&this->timerLock));

        entry.client->onTimeout(entry.timerid, entry.userdata);
        entry.client->decRef();

        expect(!pthread_mutex_lock(&this->timerLock));
    }

    expect(!pthread_mutex_unlock(&this->timerLock));
}

void TCPServer::handleEvents(int timeout)
{
    struct epoll_event events[MAX_EVENTS], *pevent;
    struct timespec monotime;
    unsigned long long timestamp, expiration = 0;
    int numEvents;

    if (!this->started)
        return;

    /* allow only calls from the main thread (which was used for the creation) */
    assert(this->mainThread == pthread_self());

    if (timeout != -1)
    {
        expect(!clock_gettime(CLOCK_MONOTONIC, &monotime));
        timestamp = monotime.tv_sec * (unsigned long long)1000 + monotime.tv_nsec / 1000000;
        expiration = timestamp + timeout;
    }

    do
    {
        numEvents = ::epoll_wait(this->epollList, events, MAX_EVENTS, this->getNextTimeout(timeout));
        if (numEvents < 0)
        {
            perror("epoll_wait error: ");
            return;
        }

        for (pevent = events; numEvents; pevent++, numEvents--)
        {
            if (!pevent->data.ptr)
            {
                if (pevent->events & EPOLLIN)
                    this->acceptConnection();
                else
                {
                    fprintf(stderr, "Error during epoll_wait, stop handling events!\n");
                    return;
                }
            }
            else if (pevent->data.ptr == (void *)(-1)) /* timer event */
            {
                if (pevent->events & EPOLLIN)
                {
                    uint64_t dummy;
                    expect(read(this->timerEvent, &dummy, sizeof(dummy)) == sizeof(dummy));
                }
            }
            else
            {
                TCPSocket *client = (TCPSocket *)pevent->data.ptr;

                /* Increase refcount to prevent destruction during event handling */
                client->incRef();

                /* receive data */
                if (pevent->events & EPOLLIN)
                    client->event_recvData();

                /* send data */
                if (pevent->events & EPOLLOUT)
                    client->event_sendData();

                if (pevent->events & EPOLLRDHUP)
                {
                    client->stopEventListener(EPOLLRDHUP);
                    client->onRemoteShutdown();
                }

                if ((pevent->events & EPOLLERR) || (pevent->events & EPOLLHUP))
                    client->disconnect(true);

                client->decRef();
            }
        }

        /* handle timer events (if any) */
        this->handleTimerEvents();

        if (timeout != -1)
        {
            expect(!clock_gettime(CLOCK_MONOTONIC, &monotime));
            timestamp = monotime.tv_sec * (unsigned long long)1000 + monotime.tv_nsec / 1000000;
            timeout = std::max(0, (int)(expiration - timestamp));
        }

    }while (timeout > 0 || timeout == -1);

}

void TCPServer::registerTimer(TCPSocket *client, int timeout, int timerid, void *userdata)
{
    std::list<TimerEntry>::iterator it;
    struct timespec monotime;
    TimerEntry entry;
    bool was_first;

    expect(!clock_gettime(CLOCK_MONOTONIC, &monotime));
    entry.client    = client;
    entry.timestamp = (monotime.tv_sec * (unsigned long long)1000 + monotime.tv_nsec / 1000000) + timeout;
    entry.timerid   = timerid;
    entry.userdata  = userdata;

    client->incRef();

    expect(!pthread_mutex_lock(&this->timerLock));

    for (it = this->timerList.begin(); it != this->timerList.end(); it++)
    {
        if (it->timestamp > entry.timestamp)
            break;
    }

    was_first = (it == this->timerList.begin());
    this->timerList.insert(it, entry);

    /* wake up main thread if the new entry is the first one in the list */
    if (was_first && this->timerEvent != (-1) && (this->mainThread != pthread_self()))
    {
        const uint64_t one = 1;
        expect(write(this->timerEvent, &one, sizeof(one)) == sizeof(one));
    }

    expect(!pthread_mutex_unlock(&this->timerLock));
}

int TCPServer::deleteTimers(TCPSocket *client, int timerid)
{
    std::list<TimerEntry>::iterator it;
    int numDeletedTimers = 0;
    bool was_first = false;

    expect(!pthread_mutex_lock(&this->timerLock));

    for (it = this->timerList.begin(); it != this->timerList.end();)
    {
        if (it->client == client && (it->timerid == timerid || timerid == -1))
        {
            was_first |= (it == this->timerList.begin());
            it->client->decRef();
            it = this->timerList.erase(it);
            numDeletedTimers++;
        }
        else
            it++;
    }

    /* wake up main thread if the first entry was modified */
    if (was_first && this->timerEvent != (-1) && (this->mainThread != pthread_self()))
    {
        uint64_t one = 1;
        expect(write(this->timerEvent, &one, sizeof(one)) == sizeof(one));
    }

    expect(!pthread_mutex_unlock(&this->timerLock));
    return numDeletedTimers;
}


bool TCPServer::start()
{
    static const int enable     = 1;
    static const int disable    = 0;

    /* already started */
    if (this->started)
        return true;

    /* no clientClass defined which can be spawend for new clients */
    if (!this->clientClass)
        return false;

    this->socket = ::socket(this->onlyIPv4 ? AF_INET : AF_INET6, SOCK_STREAM, 0);
    if (this->socket < 0)
        goto err;

    /* try to enable SO_REUSEADDR */
    ::setsockopt(this->socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));


    if (this->onlyIPv4)
    {
        struct sockaddr_in serv_addr4;
        memset(&serv_addr4, 0, sizeof(serv_addr4));
        serv_addr4.sin_family   = AF_INET;
        serv_addr4.sin_port     = htons(this->port);

        if (::bind(this->socket, (struct sockaddr *)&serv_addr4, sizeof(sockaddr_in)) < 0)
            goto err;
    }
    else
    {
        if (::setsockopt(this->socket, IPPROTO_IPV6, IPV6_V6ONLY, (void *)&disable, sizeof(int)) != 0)
            goto err;

        struct sockaddr_in6 serv_addr6;
        memset(&serv_addr6, 0, sizeof(serv_addr6));
        serv_addr6.sin6_family  = AF_INET6;
        serv_addr6.sin6_port    = htons(this->port);

        if (::bind(this->socket, (struct sockaddr *)&serv_addr6, sizeof(sockaddr_in6)) < 0)
            goto err;
    }

    if (::listen(this->socket, 5) < 0)
        goto err;

    this->epollList = ::epoll_create(10);
    if (this->epollList < 0)
        goto err;;

    this->timerEvent = ::eventfd(0, 0);
    if (this->timerEvent < 0)
        goto err;

    struct epoll_event event;
    memset(&event, 0, sizeof(event));
    event.events    = EPOLLIN;
    event.data.ptr  = NULL;

    if (::epoll_ctl(this->epollList, EPOLL_CTL_ADD, this->socket, &event) != 0)
        goto err;

    memset(&event, 0, sizeof(event));
    event.events   = EPOLLIN;
    event.data.ptr = (void *)(-1);

    if (::epoll_ctl(this->epollList, EPOLL_CTL_ADD, this->timerEvent, &event) != 0)
        goto err;

    this->started = true;
    return true;

err:
    if (this->timerEvent >= 0)
    {
        ::close(this->timerEvent);
        this->timerEvent = -1;
    }

    if (this->epollList >= 0)
    {
        ::close(this->epollList);
        this->epollList = -1;
    }

    if (this->socket >= 0)
    {
        ::close(this->socket);
        this->socket = -1;
    }

    this->started = false;
    return false;
}

void TCPServer::stop()
{
    if (!this->started)
        return;

    this->started = false;

    /* not used, but kernels before 2.6.9 need this field to be set */
    struct epoll_event event;
    memset(&event, 0, sizeof(event));
    event.events = EPOLLIN;
    ::epoll_ctl(this->epollList, EPOLL_CTL_DEL, this->socket, &event);

    /* close the socket */
    ::close(this->socket);
    this->socket = -1;

    memset(&event, 0, sizeof(event));
    event.events = EPOLLIN;
    ::epoll_ctl(this->epollList, EPOLL_CTL_DEL, this->timerEvent, &event);

    /* close the timer event socket */
    ::close(this->timerEvent);
    this->timerEvent = -1;

    /*
     * We call disconnect on every client which waits for events
     * as we do not longer handle them and the socket is therefore useless.
     * We just remove our reference without removing every single epoll entry,
     * since closing the handle to the list does the same.
     */
    std::set<TCPSocket*>::iterator it;
    for (it = this->eventClients.begin(); it != this->eventClients.end(); it++)
    {
        (*it)->disconnect(true);
        (*it)->decRef();
    }
    this->eventClients.clear();

    /* close epoll list */
    ::close(this->epollList);
    this->epollList = -1;
}

/*
    --------------- TCPSocket ---------------
*/

TCPSocket::TCPSocket()
{
    this->refcount        = 1;
    this->socket          = -1;
    this->server          = NULL;

    this->initOkay        = false;
    this->epollEvents     = 0;

    this->recvBuffer      = (unsigned char *)malloc(RECV_BUFFER_SIZE);
    this->recvBufferSize  = RECV_BUFFER_SIZE;
    this->recvBufferStart = 0;
    this->recvBufferEnd   = 0;

    this->sendBuffer      = (unsigned char *)malloc(SEND_BUFFER_SIZE);
    this->sendBufferSize  = SEND_BUFFER_SIZE;
    this->sendBufferStart = 0;
    this->sendBufferEnd   = 0;
}

TCPSocket::~TCPSocket()
{
    if (this->recvBuffer)
    {
        free(this->recvBuffer);
        this->recvBuffer = NULL;
    }

    if (this->sendBuffer)
    {
        free(this->sendBuffer);
        this->sendBuffer = NULL;
    }

    assert(this->refcount == 0);
    assert(!this->initOkay);
}


void TCPSocket::incRef()
{
    __sync_add_and_fetch(&this->refcount, 1);
}

void TCPSocket::decRef()
{
    if (__sync_add_and_fetch(&this->refcount, -1) <= 0)
        delete this;
}

bool TCPSocket::init()
{
    if (this->initOkay)
        return true;

    if (!this->recvBuffer || !this->sendBuffer)
        return false;

    if (this->socket < 0 || !this->server)
        return false;

    this->epollEvents = EPOLLIN | EPOLLRDHUP;
    if (!this->server->changeEventListener(this, this->epollEvents))
        return false;

    this->initOkay = true;
    return true;
}

bool TCPSocket::isEventActive(uint32_t eventMask)
{
    return ((this->epollEvents & eventMask) == eventMask);
}

bool TCPSocket::startEventListener(uint32_t eventMask)
{
    if (!this->initOkay)
        return false;

    uint32_t events = this->epollEvents | eventMask;
    if (this->epollEvents == events)
        return true;

    if (!this->server->changeEventListener(this, events))
        return false;

    this->epollEvents = events;
    return true;
}

bool TCPSocket::stopEventListener(uint32_t eventMask)
{
    if (!this->initOkay)
        return false;

    uint32_t events = this->epollEvents & ~eventMask;
    if (this->epollEvents == events)
        return true;

    if (!this->server->changeEventListener(this, events))
        return false;

    this->epollEvents = events;
    return true;
}

void TCPSocket::event_sendData()
{
    if (!this->initOkay)
        return;

    /* send some bytes ... */
    int length = this->sendBufferEnd - this->sendBufferStart;
    if (length)
    {
        int result = ::send(this->socket, this->sendBuffer + this->sendBufferStart, length, 0);
        if (result <= 0)
            return;

        /* adjust pointer */
        this->sendBufferStart += std::min(result, length);
    }
    else
        fprintf(stderr, "WARNING: got write ready event without data.\n");

    /* buffer is empty now */
    if (this->sendBufferStart == this->sendBufferEnd)
    {
        this->sendBufferStart = 0;
        this->sendBufferEnd   = 0;

        this->stopEventListener(EPOLLOUT);
        this->onEmptySendBuffer();
    }
}

void TCPSocket::event_recvData()
{
    if (!this->initOkay)
        return;

    while (this->recvBufferEnd + RECV_BUFFER_SIZE_MIN > this->recvBufferSize)
    {
        /* out of space, move data at the beginning to save space */
        if (this->recvBufferStart != 0)
        {
            memmove(this->recvBuffer, this->recvBuffer + this->recvBufferStart, this->recvBufferEnd - this->recvBufferStart);
            this->recvBufferEnd -= this->recvBufferStart;
            this->recvBufferStart = 0;
        }

        if (this->recvBufferEnd + RECV_BUFFER_SIZE_MIN > this->recvBufferSize)
        {
            int new_size          = std::max(this->recvBufferSize * 2, this->recvBufferEnd + RECV_BUFFER_SIZE_MIN);
            unsigned char *buffer = (unsigned char *)realloc(this->recvBuffer, new_size);

            /* critical error, not enough space to reallocate, this message is lost */
            if (buffer)
            {
                this->recvBuffer     = buffer;
                this->recvBufferSize = new_size;
            }
            else
            {
                this->onOutOfRecvBuffer();

                /* buffer was not cleaned up, retry later */
                if (this->recvBufferEnd - this->recvBufferSize + RECV_BUFFER_SIZE_MIN > this->recvBufferSize)
                    return;
            }
        }
    }

    int length = this->recvBufferSize - this->recvBufferEnd;
    ssize_t result = ::recv(this->socket, this->recvBuffer + this->recvBufferEnd, length, 0);
    if (result <= 0)
        return;

    /* adjust pointer */
    this->recvBufferEnd += std::min((int)result, length);

    /* notify application */
    length = this->recvBufferEnd - this->recvBufferStart;
    if (length > 0)
    {
        result = this->onData(this->recvBuffer + this->recvBufferStart, length);
        if (result <= 0)
            return;

        this->recvBufferStart += std::min((int)result, length);
    }

    if (this->recvBufferStart == this->recvBufferEnd)
    {
        this->recvBufferStart = 0;
        this->recvBufferEnd   = 0;
    }
}

std::string TCPSocket::getIPAddress()
{
    if (!this->initOkay)
        return "";

    struct sockaddr_storage sockAddr;
    socklen_t len = sizeof(sockaddr_storage);

    if (::getpeername(this->socket, (struct sockaddr*)&sockAddr, &len) < 0)
        return "";

    char buf[INET6_ADDRSTRLEN] = {0};
    int  port;

    if (sockAddr.ss_family == AF_INET)
    {
        struct sockaddr_in *addr = (struct sockaddr_in *)&sockAddr;
        port = ntohs(addr->sin_port);
        ::inet_ntop(AF_INET, &addr->sin_addr, buf, sizeof(buf));
    }
    else
    {
        struct sockaddr_in6 *addr = (struct sockaddr_in6 *)&sockAddr;
        port = ntohs(addr->sin6_port);

        if (IN6_IS_ADDR_V4MAPPED(&addr->sin6_addr))
        {
            struct sockaddr_in addr4;
            addr4.sin_family = AF_INET;
            addr4.sin_port   = 0;
            addr4.sin_addr.s_addr = addr->sin6_addr.s6_addr32[3];

            ::inet_ntop(AF_INET, &addr4.sin_addr, buf, sizeof(buf));
        }
        else
            ::inet_ntop(AF_INET6, &addr->sin6_addr, buf, sizeof(buf));
    }

    return std::string(buf) + ":" + intToString(port);
}

bool TCPSocket::sendData(const unsigned char *data, int length)
{
    if (!this->initOkay)
        return false;

    if (length == 0)
        return true;

    bool wasEmpty = (this->sendBufferEnd == this->sendBufferStart);

    if (this->sendBufferEnd + length > this->sendBufferSize)
    {

        /* out of space, move data at the beginning to save space */
        if (this->sendBufferStart != 0)
        {
            memmove(this->sendBuffer, this->sendBuffer + this->sendBufferStart, this->sendBufferEnd - this->sendBufferStart);
            this->sendBufferEnd -= this->sendBufferStart;
            this->sendBufferStart = 0;
        }

        if (this->sendBufferEnd + length > this->sendBufferSize)
        {
            int new_size          = std::max(this->sendBufferSize * 2, this->sendBufferEnd + length);
            unsigned char *buffer = (unsigned char *)realloc(this->sendBuffer, new_size);

            if (buffer)
            {
                this->sendBuffer     = buffer;
                this->sendBufferSize = new_size;
            }
            else
            {
                this->onOutOfSendBuffer();
                return false;
            }
        }
    }

    memcpy(this->sendBuffer + this->sendBufferEnd, data, length);
    this->sendBufferEnd += length;

    if (wasEmpty && !this->startEventListener(EPOLLOUT))
    {
        /* requesting EPOLLOUT events failed, get rid of data again */
        this->sendBufferStart = 0;
        this->sendBufferEnd   = 0;
        return false;
    }

    return true;
}

int TCPSocket::bytesSendBuffer()
{
    return this->sendBufferEnd - this->sendBufferStart;
}

int TCPSocket::bytesRecvBuffer()
{
    return this->recvBufferEnd - this->recvBufferStart;
}

void TCPSocket::disconnect(bool callOnDisconnect)
{
    if (!this->initOkay)
        return;

    this->initOkay = false;

    if (callOnDisconnect)
        this->onDisconnect();

    if (this->server)
        this->server->removeEventListener(this);

    ::close(this->socket);
}

void TCPSocket::registerTimer(int timeout, int timerid, void *userdata)
{
    return this->server->registerTimer(this, timeout, timerid, userdata);
}

int TCPSocket::deleteTimers(int timerid)
{
    return this->server->deleteTimers(this, timerid);
}
