#include "wsock/websocket.h"

#include "utils/sha1.h"
#include "utils/base64.h"
#include "utils/checkutf8.h"
#include "utils/swap.h"
#include "utils/urldecode.h"

#include <string.h>
#include <iostream>
#include <stdio.h>
#include <assert.h>

#ifdef NDEBUG
    #define expect(cond) ((void)(cond))
#else
    #define expect(cond) assert(cond)
#endif

static std::string intToString(int x)
{
    std::stringstream ss;
    ss << x;
    return ss.str();
}

inline std::string trim(std::string str)
{
    size_t pos;

    pos = str.find_first_not_of(" \f\n\r\t\v");
    if (pos != std::string::npos)
        str = str.substr(pos, std::string::npos);

    pos = str.find_last_not_of(" \f\n\r\t\v");
    if (pos != std::string::npos)
        str = str.substr(0, pos+1);

    return str;
}

static bool splitHeader(std:: string line, std::string &a, std::string &b, std::string &c, std::string splitChar = " ")
{
    size_t pos1, pos2;
    line = trim(line);

    /* find delimiters */
    if ((pos1 = line.find_first_of(splitChar)) == std::string::npos)
        return false;

    if ((pos2 = line.find_last_of(splitChar)) == std::string::npos)
        return false;

    if (pos1 == pos2)
        return false;

    a = trim(line.substr(0, pos1));
    b = trim(line.substr(pos1 + 1, pos2 - pos1 - 1));
    c = trim(line.substr(pos2 + 1, std::string::npos));
    return (a != "") && (b != "");
}

static bool splitValue(std::string line, std::string &key, std::string &value, std::string splitChar = ":")
{
    size_t pos;
    line = trim(line);

    /* find delimiter */
    if ((pos = line.find_first_of(splitChar)) == std::string::npos)
        return false;

    key     = trim(line.substr(0, pos));
    value   = trim(line.substr(pos + 1, std::string::npos));
    return (key != "");
}

/* locale independent tolower function */
inline int c_tolower(int c)
{
    if (c >= 'A' && c <= 'Z')
        c += ('a' - 'A');
    return c;
}

inline std::string lowercase(std::string str)
{
    std::string lower(str);
    std::transform(lower.begin(), lower.end(), lower.begin(), c_tolower);
    return lower;
}

WebSocket::WebSocket()
{
    this->status                 = STATUS_HTTP_REQUEST_LINE;
    this->closeOnEmptySendBuffer = false;
    this->websocketReady         = false;
    this->httpDataReady          = false;

    this->messageFragmented      = false;
    this->messageBuffer          = NULL;
    this->messageBufferSize      = 0;
    this->messageBufferStart     = 0;
    this->messageBufferEnd       = 0;
}

WebSocket::~WebSocket()
{
    if (this->messageBuffer)
    {
        free(this->messageBuffer);
        this->messageBuffer = NULL;
    }
}

bool WebSocket::sendWebsocketPackage(WebSocketOpcodes opcode, const unsigned char *data, int length)
{
    if (length > 0 && !data)
        return false;

    unsigned char buffer[sizeof(WebSocketFlags) + sizeof(uint64_t)];
    int bufferSize;

    WebSocketFlags *flags = (WebSocketFlags *)buffer;
    memset(flags, 0, sizeof(*flags));
    flags->opcode = opcode;
    flags->final  = 1;

    if (length > 65535)
    {
        flags->length = 127;
        *((uint64_t*)(buffer + sizeof(WebSocketFlags))) = bswap64(length);
        bufferSize = sizeof(WebSocketFlags) + sizeof(uint64_t);
    }
    else if (length > 125)
    {
        flags->length = 126;
        *((uint16_t*)(buffer + sizeof(WebSocketFlags))) = bswap16(length);
        bufferSize = sizeof(WebSocketFlags) + sizeof(uint16_t);
    }
    else
    {
        flags->length = length;
        bufferSize = sizeof(WebSocketFlags);
    }

    /* send the header */
    if (!this->sendData(buffer, bufferSize))
        return false;

    /* and now the data */
    if (!this->sendData(data, length))
        return false;

    return true;
}

bool WebSocket::isConnected()
{
    return this->websocketReady;
}

bool WebSocket::sendHTTPResponseData(int statusCode, std::string statusText, std::string mimeType, const unsigned char *data, int length)
{
    if (!this->httpDataReady)
        return false;

    /* It is not (yet) possible to send multiple HTTP responses */
    this->httpDataReady = false;

    std::string reponse;
    reponse  = "HTTP/1.1 " + intToString(statusCode) + " " + statusText + "\r\n";
    reponse += "Server: libwsock\r\n";
    reponse += "Connection: close\r\n";
    reponse += "Content-Type: " + mimeType + "\r\n";
    reponse += "Content-Length: " + intToString(length) + "\r\n";
    reponse += "\r\n";

    if (!this->sendData((unsigned char *)reponse.c_str(), reponse.size()))
        return false;

    if (!this->sendData(data, length))
        return false;

    return true;
}

bool WebSocket::sendHTTPResponseString(int responseCode, std::string statusText, std::string mimeType, std::string text)
{
    return this->sendHTTPResponseData(responseCode, statusText, mimeType, (unsigned char *)text.c_str(), text.size());
}

bool WebSocket::sendWebsocketData(bool text, const unsigned char *data, int length)
{
    if (!websocketReady)
        return false;

    return this->sendWebsocketPackage(text ? OPCODE_TEXTFRAME : OPCODE_BINARYFRAME, data, length);
}

bool WebSocket::sendWebsocketString(std::string text)
{
    return this->sendWebsocketData(true, (const unsigned char *)text.c_str(), text.size());
}

static inline int search_for_eol(unsigned char *data, int length)
{
    if (length < 2)
        return -1;

    for (int i = 0; i < length - 1; i++)
    {
        if (data[i] == '\r' && data[i+1] == '\n')
            return i;
    }

    return -1;
}

bool WebSocket::process_HTTPRequestLine(unsigned char *data, int length)
{
    std::string rawURL;
    std::string rawParameters;

    /* check whether the line only contains printable characters */
    for (int i = 0; i < length; i++)
    {
        if (!isprint(data[i]))
            return false;
    }

    /* convert to c++ string and split into fields */
    std::string line((char *)data, length);
    if (!splitHeader(line, this->requestType, rawURL, this->HTTPVersion))
        return false;

    /* split of arguments (if there are any) */
    if (!splitValue(rawURL, this->URL, rawParameters, "?"))
        this->URL = rawURL;

    /* try to decode url */
    if (!urlDecode(this->URL, this->URL))
        return false;

    this->parameters.clear();

    while (rawParameters.size() > 0)
    {
        std::string paramStr;
        std::string remaining;

        if (splitValue(rawParameters, paramStr, remaining, "&")){
            rawParameters = remaining;
        }else{
            paramStr = rawParameters;
            rawParameters = "";
        }

        std::string param;
        std::string value;

        if (splitValue(paramStr, value, param, "=")){
            if (!urlDecode(value, value)) continue;
            if (!urlDecode(param, param)) continue;
            this->parameters[value] = param;
        }else{
            if (!urlDecode(paramStr, paramStr)) continue;
            this->parameters[paramStr] = "";
        }
    }

    this->headerFields.clear();
    this->status = STATUS_HTTP_HEADER;
    return true;
}

bool WebSocket::process_HTTPHeaderField(unsigned char *data, int length)
{
    /* check whether the line only contains printable characters */
    for (int i = 0; i < length; i++)
    {
        if (!isprint(data[i]))
            return false;
    }

    /* convert to c++ string */
    std::string line((char *)data, length), key, value;
    if (!splitValue(line, key, value))
        return false;

    this->headerFields[lowercase(key)] = value;
    return true;
}

bool WebSocket::checkHeaderValue(std::string name, std::string value, bool can_be_substring)
{
    std::map<std::string, std::string>::iterator it = this->headerFields.find(lowercase(name));
    if (it == this->headerFields.end())
        return false;

    if (value == "")
        return true;

    std::string lowerValue = lowercase(it->second);

    if (can_be_substring)
        return (lowerValue.find(value) != std::string::npos);
    else
        return (lowerValue == value);
}

CONNECTION_TYPE WebSocket::getRequestType()
{
    /* see RFC 6455, Page 16 - 18 / 21 */
    if (lowercase(this->requestType) != "get")
        return CONNECTION_TYPE_UNKNOWN;

    std::string http;
    std::string version;
    if (!splitValue(this->HTTPVersion, http, version, "/"))
        return CONNECTION_TYPE_UNKNOWN;
    if (version != "1.0" && version != "1.1")
        return CONNECTION_TYPE_UNKNOWN;

    if (!this->checkHeaderValue("host", ""))
        return CONNECTION_TYPE_UNKNOWN;

    if (!this->checkHeaderValue("upgrade", "websocket", true))
        return CONNECTION_TYPE_HTTP;

    if (!this->checkHeaderValue("connection", "upgrade", true))
        return CONNECTION_TYPE_UNKNOWN;

    /* TODO: check length of key */
    if (!this->checkHeaderValue("sec-websocket-key", ""))
        return CONNECTION_TYPE_UNKNOWN;

    if (!this->checkHeaderValue("sec-websocket-version", "13"))
        return CONNECTION_TYPE_UNKNOWN;

    /* websocket requires HTTP/1.1 */
    if (version != "1.1")
        return CONNECTION_TYPE_UNKNOWN;

    return CONNECTION_TYPE_WEBSOCKET;
}

bool WebSocket::process_HTTPHeaderEOF()
{
    std::string reponse;
    CONNECTION_TYPE type = this->getRequestType();

    if (type == CONNECTION_TYPE_HTTP)
    {
        this->httpDataReady = true;
        this->onHTTPRequest(this->URL, this->parameters, this->headerFields);

        /* ignore new input, close as soon as sendbuffer is empty */
        this->status                    = STATUS_IGNORE_INPUT;
        this->closeOnEmptySendBuffer    = true;

        /* did the user send a response or should we fail the connection ? */
        if (!this->bytesSendBuffer())
            return false;

    }
    else if (type == CONNECTION_TYPE_WEBSOCKET)
    {
        if (!this->websocketAcceptConnection(this->URL, this->parameters, this->headerFields))
            return false;

        std::string hashInput = headerFields["sec-websocket-key"] + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"; /* SIC! */

        SHA1Context hashContext;
        uint8_t     hashResult[SHA1HashSize];

        SHA1Reset(&hashContext);
        SHA1Input(&hashContext, (uint8_t*)hashInput.c_str(), hashInput.size());
        SHA1Result(&hashContext, hashResult);

        std::string keyResponse = base64Encode((unsigned char *)&hashResult, sizeof(hashResult));

        reponse  = "HTTP/1.1 101 Switching Protocols\r\n";
        reponse += "Server: libwsock\r\n";
        reponse += "Upgrade: websocket\r\n";
        reponse += "Connection: Upgrade\r\n";
        reponse += "Sec-WebSocket-Accept: " + keyResponse + "\r\n";
        reponse += "\r\n";

        this->sendData((unsigned char *)reponse.c_str(), reponse.size());

        /* we're now in websocket parsing mode */
        this->status         = STATUS_WEBSOCKET_FLAGS;
        this->websocketReady = true;

        this->onWebsocketConnect();

        /* initialize all flags */
        this->messageFragmented  = false;
        this->messageBufferStart = 0;
        this->messageBufferEnd   = 0;
    }
    else
    {
        return false;
    }

    return true;
}

bool WebSocket::checkWebsocketFlags()
{
    /* reserved flags must be zero */
    if (this->flags.reserved1 || this->flags.reserved2 || this->flags.reserved3)
        return false;

    /*  %x3-7 are reserved for further non-control frames */
    if (this->flags.opcode >= 0x3 && this->flags.opcode <= 0x7)
        return false;

    /* %xB-F are reserved for further control frames */
    if (this->flags.opcode >= 0xB)
        return false;

    /* client frames must be masked */
    if (!this->flags.mask)
        return false;

    /* control frames may be injected at any time (even during fragmented messages) */
    if (this->flags.opcode >= OPCODE_CLOSECONNECTION)
    {

        /* control frames must not be fragmented */
        if (!this->flags.final)
            return false;

    }
    else if (this->messageFragmented)
    {

        /* only the first fragment contains the real opcode */
        if (this->flags.opcode != OPCODE_CONTINUATION)
            return false;

    }
    else /* if !this->messageFragmented */
    {

        /* the first fragmented packet must contain a valid opcode */
        if (!this->flags.final && this->flags.opcode != OPCODE_TEXTFRAME && this->flags.opcode != OPCODE_BINARYFRAME)
            return false;

    }

    return true;
}

bool WebSocket::checkWebsocketLength()
{
    /* restrict size of package */
    if (this->payloadLength >= WEBSOCKET_MAX_MESSAGE_LENGTH)
        return false;

    /* contols frame must not have more than 125 byte of paylod */
    if (this->flags.opcode >= OPCODE_CLOSECONNECTION && this->payloadLength > 125)
        return false;



    return true;
}

bool WebSocket::process_WebsocketPayloadData(unsigned char *data, int length)
{

    if (length == 0)
        return true;

    /* handle control frames differently */
    if (this->flags.opcode >= OPCODE_CLOSECONNECTION)
    {

        /* append data at the end of the control buffer */
        assert(this->controlBufferPos + length <= (signed)sizeof(this->controlBuffer));
        memcpy(this->controlBuffer + this->controlBufferPos, data, length);
        this->controlBufferPos += length;

    }
    else
    {

        if (this->messageBufferEnd + length > this->messageBufferSize)
        {
            int new_size = std::max(this->messageBufferSize * 2, this->messageBufferEnd + length);
            unsigned char *buffer = (unsigned char *)realloc(this->messageBuffer, new_size);

            if (buffer)
            {
                this->messageBuffer     = buffer;
                this->messageBufferSize = new_size;
            }
            else
                return false;
        }

        memcpy(this->messageBuffer + this->messageBufferEnd, data, length);
        this->messageBufferEnd += length;
    }

    /* reduce the number of expected payload bytes */
    this->payloadLength -= length;

    return true;
}

static inline void unmask_xor_data(unsigned char *data, int length, uint32_t mask)
{
    uint32_t *p32 = (uint32_t *)data;

    for (; length >= 4; p32++, length -= 4)
        *p32 ^= mask;

    if (length > 0)
    {
        uint32_t temp;
        memcpy(&temp, p32, length);
        temp ^= mask;
        memcpy(p32, &temp, length);
    }

}

bool WebSocket::process_WebsocketPayloadEOF()
{
    /* by default we expect a new websocket flags package */
    this->status = STATUS_WEBSOCKET_FLAGS;

    /* handle control frames differently */
    if (this->flags.opcode >= OPCODE_CLOSECONNECTION)
    {
        unmask_xor_data(this->controlBuffer, this->controlBufferPos, this->maskKey);

        /* should have already been checked earlier */
        assert(this->flags.final);

        if (this->flags.opcode == OPCODE_CLOSECONNECTION)
        {
            this->websocketReady = false;
            this->onWebsocketDisconnect();

            sendWebsocketPackage(OPCODE_CLOSECONNECTION, NULL, 0);
            this->closeOnEmptySendBuffer = true;

            this->status = STATUS_IGNORE_INPUT;
            return true;
        }
        else if (this->flags.opcode == OPCODE_PING)
            return sendWebsocketPackage(OPCODE_PONG, this->controlBuffer, this->controlBufferPos);
        else
            return (this->flags.opcode == OPCODE_PONG);

    }
    else if (this->flags.opcode == OPCODE_TEXTFRAME || this->flags.opcode == OPCODE_BINARYFRAME ||
            (this->flags.opcode == OPCODE_CONTINUATION && this->messageFragmented))
    {
        /* unmask the data from this frame */
        unmask_xor_data(this->messageBuffer + this->messageBufferStart, this->messageBufferEnd - this->messageBufferStart, this->maskKey);

        if (this->flags.final)
        {

            if (this->messageOpcode == OPCODE_TEXTFRAME)
            {
                if (!isValidUTF8(this->messageBuffer, this->messageBufferEnd))
                    return false;
            }

            /* handle this message */
            this->onWebsocketData( (this->messageOpcode == OPCODE_TEXTFRAME), this->messageBuffer, this->messageBufferEnd );

            /* reset fragmentation flag */
            this->messageBufferStart = 0;
            this->messageBufferEnd   = 0;
            this->messageFragmented  = false;
        }
        else
            this->messageBufferStart = this->messageBufferEnd;

        return true;
    }

    return false;
}

void WebSocket::onConnect()
{
    /*
    std::cout << "Client connected with IP: " << this->getIPAddress() << std::endl;
    */
}

ssize_t WebSocket::onData(unsigned char *data, int length)
{
    unsigned char *start_data = data;

    while (length > 0)
    {
        switch (this->status)
        {
            case STATUS_IGNORE_INPUT:
            {
                data += length;
                length = 0;
                goto out;
            }

            case STATUS_HTTP_REQUEST_LINE:
            case STATUS_HTTP_HEADER:
            {
                int eol = search_for_eol(data, length);
                if (eol < 0)
                    goto out;

                bool success;
                if (status == STATUS_HTTP_REQUEST_LINE)
                    success = this->process_HTTPRequestLine(data, eol);
                else if (eol > 0)
                    success = this->process_HTTPHeaderField(data, eol);
                else
                    success = this->process_HTTPHeaderEOF();

                data   += (eol + 2);
                length -= (eol + 2);

                if (!success)
                    goto err;

                break;
            }

            case STATUS_WEBSOCKET_FLAGS:
            {
                if (length < (signed)sizeof(WebSocketFlags))
                    goto out;

                memcpy(&this->flags, data, sizeof(WebSocketFlags));

                data   += sizeof(WebSocketFlags);
                length -= sizeof(WebSocketFlags);

                if (!this->checkWebsocketFlags())
                    goto err;

                if (this->flags.opcode == OPCODE_TEXTFRAME ||
                    this->flags.opcode == OPCODE_BINARYFRAME)
                {
                    if (this->messageFragmented)
                        goto err;

                    this->messageFragmented = (this->flags.final == 0);
                    this->messageOpcode     = this->flags.opcode;
                }

                if (this->flags.length <= 125)
                {
                    this->payloadLength = this->flags.length;
                    this->status = STATUS_WEBSOCKET_MASKKEY;
                }
                else if (this->flags.length == 126)
                    this->status = STATUS_WEBSOCKET_SIZE16;
                else if (this->flags.length == 127)
                    this->status = STATUS_WEBSOCKET_SIZE64;

                break;
            }

            case STATUS_WEBSOCKET_SIZE16:
            {
                if (length < (signed)sizeof(uint16_t))
                    goto out;

                this->payloadLength = bswap16(*(uint16_t *)data);

                data   += sizeof(uint16_t);
                length -= sizeof(uint16_t);

                this->status = STATUS_WEBSOCKET_MASKKEY;
                break;
            }

            case STATUS_WEBSOCKET_SIZE64:
            {
                if (length < (signed)sizeof(uint64_t))
                    goto out;

                this->payloadLength = bswap64(*(uint64_t *)data);

                data   += sizeof(uint64_t);
                length -= sizeof(uint64_t);

                this->status = STATUS_WEBSOCKET_MASKKEY;

                break;
            }

            case STATUS_WEBSOCKET_MASKKEY:
            {
                if (length < (signed)sizeof(uint32_t))
                    goto out;

                this->maskKey = *(uint32_t *)data;

                data   += sizeof(uint32_t);
                length -= sizeof(uint32_t);

                if (!this->checkWebsocketLength())
                    goto err;

                /* reset control buffer pos */
                this->controlBufferPos = 0;

                if (this->payloadLength)
                    this->status = STATUS_WEBSOCKET_PAYLOAD;
                else
                {
                    if (!this->process_WebsocketPayloadEOF())
                        goto err;
                }

                break;
            }

            case STATUS_WEBSOCKET_PAYLOAD:
            {
                int size = std::min(length, (signed)this->payloadLength);

                if (!this->process_WebsocketPayloadData(data, size))
                    goto err;

                data    += size;
                length  -= size;

                if (!this->payloadLength)
                {
                    if (!this->process_WebsocketPayloadEOF())
                        goto err;
                }

                break;
            }

            default:
            {
                std::cerr << "ERROR: Unhandled internal parser state " << this->status << std::endl;
                goto err;
            }
        }
    }

out:
    return data - start_data;

err:
    /* skip input */
    data += length;
    length = 0;

    /* Something went wrong - if a connection was established before we try to close it the proper way */
    if (this->websocketReady)
    {
        this->websocketReady = false;
        this->onWebsocketDisconnect();

        sendWebsocketPackage(OPCODE_CLOSECONNECTION, NULL, 0);
        this->closeOnEmptySendBuffer = true;
    }
    else
        this->disconnect(true);

    this->status = STATUS_IGNORE_INPUT;

    return data - start_data;
}

void WebSocket::onRemoteShutdown()
{
    if (this->websocketReady)
    {
        this->websocketReady = false;
        this->onWebsocketDisconnect();
    }

    if (this->bytesSendBuffer() > 0)
        this->closeOnEmptySendBuffer = true;
    else
        this->disconnect(true);
}

void WebSocket::onDisconnect()
{
    if (this->websocketReady)
    {
        this->websocketReady = false;
        this->onWebsocketDisconnect();
    }
}

void WebSocket::onEmptySendBuffer()
{
    if (this->closeOnEmptySendBuffer)
        this->disconnect(true);
}

void WebSocket::onOutOfSendBuffer()
{
    std::cerr << "ERROR: out of send buffer!" << std::endl;
    assert(0);
}

void WebSocket::onOutOfRecvBuffer()
{
    std::cerr << "ERROR: out of recv buffer!" << std::endl;
    assert(0);
}