#pragma once

#include "tcpsock.h"
#include <stdint.h>
#include <map>
#include <sstream>
#include <algorithm>

enum WEBSOCKET_STATUS
{
    STATUS_IGNORE_INPUT,
    STATUS_HTTP_REQUEST_LINE,
    STATUS_HTTP_HEADER,
    STATUS_WEBSOCKET_FLAGS,
    STATUS_WEBSOCKET_SIZE16,
    STATUS_WEBSOCKET_SIZE64,
    STATUS_WEBSOCKET_MASKKEY,
    STATUS_WEBSOCKET_PAYLOAD
};

enum CONNECTION_TYPE
{
    CONNECTION_TYPE_UNKNOWN,
    CONNECTION_TYPE_HTTP,
    CONNECTION_TYPE_WEBSOCKET
};

#define WEBSOCKET_BUFFER_SIZE 8192
#define MAX_BUFFER_LENGTH 8192
#define WEBSOCKET_MAX_MESSAGE_LENGTH 1024 * 1024 * 10 //10 MB

struct WebSocketFlags
{
    unsigned char opcode    : 4;
    unsigned char reserved3 : 1;
    unsigned char reserved2 : 1;
    unsigned char reserved1 : 1;
    unsigned char final     : 1;

    unsigned char length    : 7;
    unsigned char mask      : 1;
};

enum WebSocketOpcodes
{
    OPCODE_CONTINUATION     = 0x0,
    OPCODE_TEXTFRAME        = 0x1,
    OPCODE_BINARYFRAME      = 0x2,
    OPCODE_CLOSECONNECTION  = 0x8,
    OPCODE_PING             = 0x9,
    OPCODE_PONG             = 0xA
};

class WebSocket : public TCPSocket
{
    private:
        WEBSOCKET_STATUS status;
        bool closeOnEmptySendBuffer;
        bool websocketReady;
        bool httpDataReady;

        /* request information */
        std::string requestType;
        std::string URL;
        std::string HTTPVersion;
        std::map<std::string, std::string> parameters;
        std::map<std::string, std::string> headerFields;

        /* websocket frame information */
        WebSocketFlags flags;
        uint64_t payloadLength;
        uint32_t maskKey;

        /* websocket message information */
        unsigned int  messageOpcode;
        bool          messageFragmented;

        unsigned char *messageBuffer;
        int           messageBufferSize;
        int           messageBufferStart;
        int           messageBufferEnd;

        /* control message buffer */
        unsigned char controlBuffer[128]; /* min 125! */
        int           controlBufferPos;

        bool sendWebsocketPackage(WebSocketOpcodes opcode, const unsigned char *payload, int length);

        bool checkHeaderValue(std::string name, std::string value, bool can_be_substring = false);
        bool checkWebsocketFlags();
        bool checkWebsocketLength();
        CONNECTION_TYPE getRequestType();

        bool process_HTTPRequestLine(unsigned char *data, int length);
        bool process_HTTPHeaderField(unsigned char *data, int length);
        bool process_HTTPHeaderEOF();
        bool process_WebsocketPayloadData(unsigned char *data, int length);
        bool process_WebsocketPayloadEOF();

    protected:
        virtual ~WebSocket();

    public:

        WebSocket();

        bool isConnected();

        bool sendHTTPResponseData(int responseCode, std::string statusText, std::string mimeType, const unsigned char *data, int length);
        bool sendHTTPResponseString(int responseCode, std::string statusText, std::string mimeType, std::string text);

        bool sendWebsocketData(bool text, const unsigned char *data, int length);
        bool sendWebsocketString(std::string text);

        /* methods provided by the user */
        virtual TCPSocket* create()                                                                                         = 0;
        virtual void onHTTPRequest(std::string url, const std::map<std::string, std::string> &parameters, const std::map<std::string, std::string> &headerFields)               = 0;
        virtual bool websocketAcceptConnection(std::string url, const std::map<std::string, std::string> &parameters, const std::map<std::string, std::string> &headerFields)   = 0;
        virtual void onWebsocketConnect()                                                                                   = 0;
        virtual void onWebsocketData(bool text, unsigned char *data, int length)                                            = 0;
        virtual void onWebsocketDisconnect()                                                                                = 0;

        /* internal methods implemented by the library - not meant for external usage */
        virtual void onConnect();
        virtual ssize_t onData(unsigned char *data, int length);
        virtual void onRemoteShutdown();
        virtual void onDisconnect();

        virtual void onEmptySendBuffer();
        virtual void onOutOfSendBuffer();
        virtual void onOutOfRecvBuffer();
};