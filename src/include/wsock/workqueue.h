#pragma once

#include "wsock/websocket.h"
#include <string>
#include <vector>
#include <list>
#include <set>
#include <stdint.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>

#define THREADSAFE /*thread-safe*/

class WorkItem
{
    private:
        int refcount;
        unsigned int waiters;
        int event;

    protected:
        /* The WorkItem is destroyed by calling defRef() */
        virtual ~WorkItem();

    public:
        WorkItem();

        void THREADSAFE incRef();
        void THREADSAFE decRef();

        /* used to signal that the work item has been finished */
        bool THREADSAFE isSignalled();
        void THREADSAFE wait();
        void THREADSAFE signal();
        void THREADSAFE reset();
};

struct WaitEntry
{
    TCPSocket *client;
    int timerid;
};

class WorkQueue
{
    private:
        pthread_mutex_t         queueLock;
        std::list<WorkItem*>    queueList;
        std::list<WaitEntry>    waitList;

    public:
        WorkQueue();

        /* append new work - keeps a reference */
        void THREADSAFE appendWork(WorkItem *item);
        void THREADSAFE requeueWork(WorkItem *item);

        /* dequeue a work item, or register notification via TCPSocket callback */
        WorkItem* THREADSAFE tryGetWork(TCPSocket *client = NULL, int timerid = -1);

        /* delete queued callbacks */
        int THREADSAFE deleteTimers(TCPSocket *client, int timerid);

        ~WorkQueue();
};