#pragma once

#include <string>
#include <vector>
#include <list>
#include <set>
#include <stdint.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>

#define RECV_BUFFER_SIZE 4096
#define RECV_BUFFER_SIZE_MIN 1024
#define SEND_BUFFER_SIZE 4096
#define THREADSAFE /*thread-safe*/

class TCPSocket;

struct TimerEntry
{
    TCPSocket *client;
    unsigned long long timestamp;
    int timerid;
    void *userdata;
};

class TCPServer
{
    friend class TCPSocket;

    private:
        pthread_t             mainThread;

        TCPSocket             *clientClass;
        int                   socket;
        int                   port;
        int                   onlyIPv4;
        bool                  started;
        int                   epollList;
        std::set<TCPSocket*>  eventClients;

        int                   timerEvent;
        pthread_mutex_t       timerLock;
        std::list<TimerEntry> timerList;

        void acceptConnection();
        bool changeEventListener(TCPSocket *client, uint32_t events);
        void removeEventListener(TCPSocket *client);
        int  getNextTimeout(int timeout);
        void handleTimerEvents();

    public:
        TCPServer(TCPSocket *clientClass, int port, bool onlyIPv4 = false);

        /* timeout: -1 - wait forever, 0 - return immediately, >0 - wait for ... ms */
        void handleEvents(int timeout = -1);

        /* timer functions */
        void THREADSAFE registerTimer(TCPSocket *client, int timeout, int timerid, void *userdata);
        int  THREADSAFE deleteTimers(TCPSocket *client, int timerid = -1);

        /* start / stop server */
        bool start();
        void stop();

        ~TCPServer();
};

class TCPSocket
{
    friend class TCPServer;

    private:
        int             refcount;
        int             socket;
        TCPServer       *server;

        bool            initOkay;
        uint32_t        epollEvents;

        unsigned char   *recvBuffer;
        int             recvBufferSize;
        int             recvBufferStart;
        int             recvBufferEnd;

        unsigned char   *sendBuffer;
        int             sendBufferSize;
        int             sendBufferStart;
        int             sendBufferEnd;

        bool init();

        /* start/stop listening for events */
        bool isEventActive(uint32_t eventMask);
        bool startEventListener(uint32_t eventMask);
        bool stopEventListener(uint32_t eventMask);

        /* send the next block of data */
        void event_sendData();
        void event_recvData();

    protected:
        /* The socket is destroyed by calling decRef() */
        virtual ~TCPSocket();

    public:
        TCPSocket();

        void THREADSAFE incRef();
        void THREADSAFE decRef();

        std::string THREADSAFE getIPAddress();
        bool sendData(const unsigned char *data, int length);
        void disconnect(bool callOnDisconnect);

        /* timer functions */
        void THREADSAFE registerTimer(int timerid, int timeout, void *userdata);
        int  THREADSAFE deleteTimers(int timerid = -1);

        /* number of bytes in buffers */
        int  bytesSendBuffer();
        int  bytesRecvBuffer();

        /* methods provided by the user */
        virtual TCPSocket* create()                             = 0;
        virtual void onConnect()                                = 0;
        virtual ssize_t onData(unsigned char *data, int length) = 0;
        virtual void onTimeout(int timerid, void *userdata)     = 0;
        virtual void onRemoteShutdown()                         = 0;
        virtual void onDisconnect()                             = 0;

        virtual void onEmptySendBuffer()                        = 0;
        virtual void onOutOfSendBuffer()                        = 0;
        virtual void onOutOfRecvBuffer()                        = 0;
};

