.PHONY: clean install uninstall
SUBDIRS= src examples

CXX := g++
CXXFLAGS=-Wall -fPIC -O2
prefix := /usr

export

all: $(SUBDIRS)

install: all
	mkdir -p "$(DESTDIR)$(prefix)/include/wsock"
	mkdir -p "$(DESTDIR)$(prefix)/lib/"

	install -m 0644 src/include/wsock/tcpsock.h "$(DESTDIR)$(prefix)/include/wsock/tcpsock.h"
	install -m 0644 src/include/wsock/websocket.h "$(DESTDIR)$(prefix)/include/wsock/websocket.h"
	install -m 0644 src/include/wsock/workqueue.h "$(DESTDIR)$(prefix)/include/wsock/workqueue.h"

	install -m 0644 src/libwsock.so.0.1 "$(DESTDIR)$(prefix)/lib/libwsock.so.0.1"

	rm -f "$(DESTDIR)$(prefix)/lib/libwsock.so"
	ln -s "$(DESTDIR)$(prefix)/lib/libwsock.so.0.1" "$(DESTDIR)$(prefix)/lib/libwsock.so"

uninstall: all
	rm -f "$(DESTDIR)$(prefix)/include/wsock/tcpsock.h"
	rm -f "$(DESTDIR)$(prefix)/include/wsock/websocket.h"
	rm -f "$(DESTDIR)$(prefix)/include/wsock/workqueue.h"

	rm -f "$(DESTDIR)$(prefix)/lib/libwsock.so.0.1"
	rm -f "$(DESTDIR)$(prefix)/lib/libwsock.so"

	rmdir --ignore-fail-on-non-empty "$(DESTDIR)$(prefix)/lib/"
	rmdir --ignore-fail-on-non-empty "$(DESTDIR)$(prefix)/include/"

.PHONY: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@

clean:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir $@; \
	done

