# Libwsock

Libwsock is a simple C++ library for Linux to make the implementation of
Websocket servers very easy. The library does all the hard work and you just
need to derive your own class from `WebSocket`, which is used to represent a
websocket connection. Just provide your own implementation of functions like
`onWebsocketData` and you are ready to start your first server.

The library is very lightweight does not have any dependencies. It can handle
IPv4 as well as IPv6 connections. While the server implementation is only single
threaded, it is very efficient due to the usage of epoll. Moreover the library
provides a multi threading safe workqueue that can be used to exchange data with
other threads. It is also possible to register timers that can be used to
execute code at a later time.

The code is licensed under the BSD-2 license.

## Getting Started

The first step is to compile the code via `make`. Afterwards it is recommended
to explore the provided examples to get a better understanding of libwsock. It
is not necessary to install the library, the examples can be started directly
from the source tree.

### examples/echoserver

The most basic example is the echoserver. It simply sends back all data it
receives. The server does also utilize the timer mechanism provided by libwsock
to send a Hello World message to all connected clients every 5 seconds. This
timer mechanism can, for example, be utilized in real world applications to kick
idle clients. In order to try out the example start the server `./echoserver`
and go to https://www.websocket.org/echo.html. Enter `ws://127.0.0.1:8377` as
address and try it out.

### examples/chatserver

This example demonstrates how a simple browser chat can be implemented using
libwsock. Each user is represented by an instance of `WebSocketClient` and added
to a global list of connected users. When a user sends a message, it is
forwarded to all other connected users. In order to play with the example start
`./chatserver` and open `chatclient.html` one or multiple times in your browser.
Select a nick and start chatting!

### examples/workqueue

This example explains how workqueues can be used in libwsock. While the original
intention is to pass data between the websocket thread and other threads, the
system is used here to exchange jobs between connected clients. Each connected
user can either submit or request a new job. If no job is available upon
request, the workqueue is asked to trigger a timer event when a new job becomes
available. The `onTimeout` functions then receives a timeout event with the job
object as user data pointer. To try out the example, start `./workqueque` and
open `client.html` one or multiple times in your browser.

## Further Hints

The implementation is mostly RFC conform with one exception: For security
reasons the server closes the connection when a client announces a package
with a size greater or equal to `WEBSOCKET_MAX_MESSAGE_LENGTH` (currently 10
MB). This should prevent clients from filling up all available memory by
announcing big packages without ever sending their full content.

The current implementation does not support TLS connections as this would pull
in further dependencies. If a TLS protection is desired, we recommend to use
NGINX as reverse proxy.
